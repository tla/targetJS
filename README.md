# Beta Version

# targetJS
cross-browser CSS3 selector

# support :
- chrome 10+
- IE 10+
- firefox 4+
- safari 5+
- opera 10+

# examples
- _T( " tag " )
- _T( " #id " )
- _T( " .class " )
- _T( " .class:first " )
- _T( " .class:last " )
- _T( " tag:has( selector ) " )
- _T( " body:has( p:has( span:not( .class ) ) ) " )
- _T( " selector:childlist:checked " )
- _T( " selector :first :has([class]) " )
- _T( " [attribute='value'] " )
- _T( " [attribute!='value'] " )
- _T( " [!attribute] " )
- _T( " tag:not( .class-one , .class-two ) " )
- _T( " tag:not( .class:not( [attribute!='value'] ) , #id[attribute!='value']:not( :focus ) ) " )
- _T( " selector " , document.getElementById('container') )


- _T( htmlElement )
- _T( [nodelist] )


- _T.match( document.body , 'body' ) // true
- _T.closest( HTMLElement , 'body' ) // [***]

# old browser CSS3 selector support :
- \*
- tag
- \#id
- .class
- selector > selector
- selector + selector
- selector ~ selector
- selector[attr]
- selector[attr=value]
- selector[attr~=value]
- selector[attr|=value]
- selector[attr^=value]
- selector[attr*=value]
- selector[attr$=value]
- :link
- :root
- :valid
- :empty
- :active
- :target
- :invalid
- :visited
- :checked
- :enabled
- :in-range
- :optional
- :required
- :disabled
- :read-only
- :last-child
- :only-child
- :read-write
- :last-of-type
- :only-of-type
- :out-of-range
- :first-of-type
- :lang( string )
- :nth-child( formula )
- :nth-of-type( formula )
- :nth-last-child( formula )
- :nth-last-of-type( formula )

# addons :
- selector-a:previous-all( [ selector-b ] )               -- select selector-b placed before selector-a
- selector-a:next-all( [ selector-b ] )                   -- select selector-b placed after selector-a
- selector-a:previous( [ selector-b ] )                   -- select selector-b placed immediatly before selector-a
- selector-a:next( [ selector-b ] )                       -- select selector-b placed immediatly after selector-a
- selector-a:before-all( selector-b )                 -- select selector-a placed before selector-b
- selector-a:after-all( selector-b )                  -- select selector-a placed after selector-b
- selector-a:before( selector-b )                     -- select selector-a placed immediatly before selector-b
- selector-a:after( selector-b )                      -- select selector-a placed immediatly after selector-b
- selector-a:find( selector-b )                       -- select selector-a childNodes recurcively
- :children( [ selector ] )                           -- select childNodes ( all childnodes if empty )
- :has( selector )                                    -- selector:has( child selector )
- :closest( [ selector ] )                            -- select parent ( parents chain if empty )
- selector-a < selector-b                             -- select first parent
- :parent( [ selector ] )                             -- select first parent
- :contains-cs( text )                                -- contains a specific text ( case sensitive ) (2)
- :contains( text )                                   -- contains a specific text (2)
- selector-a:is-child-of( selector-b )                -- select selector-a if it's childnode of selector-b
- :unselected                                         -- ex : "select > option:unselected"
- :draggable                                          -- draggable element
- :unchecked                                          -- !:checked
- :is-parent                                          -- the target has a child
- :animated                                           -- animated element with css3 ( the browser need to support it )
- :selected                                           -- ex : "select > option:selected"
- :no-value                                           -- input without value (1 - 2)
- :value                                              -- input with a value (1 - 2)
- :first                                              -- first result ( not first-child )
- :last                                               -- last result ( not last-child )
- :blur                                               -- !:focus
- selector[!attr]                                     -- without attribute
- selector[-attr]                                     -- attribute with a value (2)
- selector[!-attr]                                    -- attribute without a value (2)

# filters :
_T( nodelist , HTMLElement ); -- only childs of container
_T( nodelist , selector ); -- only elements who matchs with selector

# input shortcut selector :
- :datetime-local
- :checkbox
- :password
- :datetime
- :hidden
- :submit
- :number
- :search
- :image
- :color
- :range
- :month
- :reset
- :radio
- :email
- :date
- :file
- :text
- :time
- :week
- :tel
- :url

# shortcut selector :
- :odd
- :even

# note
- (1) : /!\ input[type="checkbox"], checked or not, HAS a value
- (2) : without white-space, so "      " is like empty