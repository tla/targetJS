/**
 * @author tla
 * @see {@link https://framagit.org/tla}
 *
 * targetJS
 * @see {@link https://framagit.org/tla/targetJS}
 *
 * @license MIT
 * @see {@link https://framagit.org/tla/targetJS/blob/master/LICENSE}
 *
 * @use js-tools-kit
 * @see {@link https://framagit.org/tla/js-tools-kit}
 *
 * BETA VERSION
 * cross-browser CSS3 selector
 *
 * @method _T - css selector
 * @argument {string|nodelist} s - css selector
 * @argument {HTMLElement} [o = document] - container
 * @return array | null
 *
 * @method _T.match - if element match with a css selector
 * @argument [HTMLElement] - element
 * @argument [string] - selector
 * @return boolean
 * */
( function () {
    
    'use strict';
    
    var global ,
        hasProperty = Object.prototype.hasOwnProperty;
    
    try {
        global = Function( 'return this' )() || ( 42, eval )( 'this' );
    } catch ( e ) {
        global = window;
    }
    
    /**
     * js-tools-kit
     * @license MIT
     * @see {@link https://framagit.org/tla/js-tools-kit}
     * */
    ( function () {
        
        function _defineProperty ( obj , key , prop ) {
            Object.defineProperty( obj , key , {
                configurable : false ,
                enumerable : true ,
                writable : false ,
                value : prop
            } );
        }
        
        /**
         * @readonly
         * @property {Window} global
         * */
        _defineProperty( global , 'global' , global );
        
        /**
         * @readonly
         * @function _defineProperty
         * @param {Object} obj
         * @param {string} key
         * @param {*} prop
         * */
        _defineProperty( global , '_defineProperty' , _defineProperty );
        
        /**
         * @readonly
         * @function isset
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isset' , function ( _ ) {
            return _ !== undefined;
        } );
        
        /**
         * @readonly
         * @function isnull
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isnull' , function ( _ ) {
            return _ === null;
        } );
        
        /**
         * @readonly
         * @function exist
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'exist' , function ( _ ) {
            return isset( _ ) && !isnull( _ );
        } );
        
        /**
         * @readonly
         * @function isdate
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isdate' , function ( _ ) {
            return exist( _ ) && _ instanceof Date;
        } );
        
        /**
         * @readonly
         * @function isarray
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isarray' , function ( _ ) {
            return exist( _ ) && Array.isArray( _ );
        } );
        
        /**
         * @readonly
         * @function isobject
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isobject' , function ( _ ) {
            return exist( _ ) &&
                _ instanceof Object && _.toString() === '[object Object]' &&
                _.constructor.hasOwnProperty( 'defineProperty' );
        } );
        
        /**
         * @readonly
         * @function isstring
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isstring' , function ( _ ) {
            return exist( _ ) && ( typeof _ === 'string' || _ instanceof global.String );
        } );
        
        /**
         * @readonly
         * @function isfillstring
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isfillstring' , function ( _ ) {
            return isstring( _ ) && !!_.trim();
        } );
        
        /**
         * @readonly
         * @function isboolean
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isboolean' , function ( _ ) {
            return exist( _ ) && ( typeof _ === 'boolean' || _ instanceof global.Boolean );
        } );
        
        /**
         * @readonly
         * @function isinteger
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isinteger' , function ( _ ) {
            return exist( _ ) && Number.isInteger( _ );
        } );
        
        /**
         * @readonly
         * @function isfloat
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isfloat' , function ( _ ) {
            return exist( _ ) && typeof _ == 'number' && isFinite( _ );
        } );
        
        /**
         * @readonly
         * @function isfunction
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isfunction' , function ( _ ) {
            return exist( _ ) && ( typeof _ === 'function' || _ instanceof global.Function );
        } );
        
        /**
         * @readonly
         * @function isevent
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isevent' , function ( _ ) {
            if ( exist( _ ) ) {
                return ( _ instanceof global.Event ) ||
                    ( exist( _.defaultEvent ) && _.defaultEvent instanceof global.Event ) ||
                    ( exist( _.originalEvent ) && _.originalEvent instanceof global.Event );
            }
            return false;
        } );
        
        /**
         * @readonly
         * @function isregexp
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isregexp' , function ( _ ) {
            return exist( _ ) && ( _ instanceof global.RegExp );
        } );
        
        /**
         * @readonly
         * @function isnodelist
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isnodelist' , function ( _ ) {
            return exist( _ ) && (
                ( _ instanceof global.NodeList || _ instanceof global.HTMLCollection ) ||
                ( !isset( _.slice ) && isset( _.length ) && typeof _ === 'object' )
            );
        } );
        
        /**
         * @readonly
         * @function isdocument
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isdocument' , function ( _ ) {
            return exist( _ ) && exist( _.defaultView ) && _ instanceof _.defaultView.HTMLDocument;
        } );
        
        /**
         * @readonly
         * @function iswindow
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'iswindow' , function ( _ ) {
            return exist( _ ) && ( _ === global || ( exist( _.window ) &&
                exist( _.window.constructor ) && _ instanceof _.window.constructor ) );
        } );
        
        /**
         * @readonly
         * @function getdocument
         * @param {*} _
         * @return {?HTMLDocument}
         * */
        _defineProperty( global , 'getdocument' , function ( _ ) {
            var tmp;
            
            if ( exist( _ ) ) {
                
                if ( isdocument( _ ) ) {
                    return _;
                }
                
                if ( isdocument( tmp = _.ownerDocument ) ) {
                    return tmp;
                }
                
                if ( iswindow( _ ) ) {
                    return _.document;
                }
                
            }
            
            return null;
        } );
        
        /**
         * @readonly
         * @function getwindow
         * @param {*} _
         * @return {!Window}
         * */
        _defineProperty( global , 'getwindow' , function ( _ ) {
            var tmp;
            
            if ( iswindow( _ ) ) {
                return _;
            }
            
            if ( tmp = getdocument( _ ) ) {
                return tmp.defaultView;
            }
            
            return null;
        } );
        
        /**
         * @readonly
         * @function isnode
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isnode' , function ( _ ) {
            var doc , tag;
            
            if ( ( doc = getdocument( _ ) ) && isstring( tag = _.tagName ) ) {
                return ( doc.createElement( tag ) instanceof _.constructor ||
                    getdocument( global ).createElement( tag ) instanceof _.constructor );
            }
            
            return false;
        } );
        
        /**
         * @readonly
         * @function isfragment
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isfragment' , function ( _ ) {
            var win;
            
            return ( win = getwindow( _ ) ) &&
                ( _ instanceof win.DocumentFragment || _ instanceof global.DocumentFragment );
        } );
        
        /**
         * @readonly
         * @property {boolean} eventPassiveSupport
         * */
        ( function () {
            var tmp ,
                sup = false ,
                document = getdocument( global );
            
            if ( document ) {
                tmp = document.createDocumentFragment();
                
                var empty = function () {} , setting = {
                    get passive () {
                        sup = true;
                        return false;
                    }
                };
                
                tmp.addEventListener( 'click' , empty , setting );
            }
            
            _defineProperty( global , 'eventPassiveSupport' , sup );
        } )();
        
        /**
         * @readonly
         * @function domLoad
         * @param {function} callback
         * */
        _defineProperty( global , 'domLoad' , ( function () {
            var ready = false ,
                stackfunction = [] ,
                document = getdocument( global );
            
            function stack ( callback ) {
                if ( isfunction( callback ) ) {
                    if ( ready ) {
                        return callback.call( document );
                    }
                    
                    stackfunction.push( callback );
                }
            }
            
            function load () {
                if ( !ready ) {
                    ready = true;
                    
                    stackfunction.forEach( function ( callback ) {
                        callback.call( document );
                    } );
                    
                    stackfunction = null;
                    
                    if ( document ) {
                        document.removeEventListener( 'DOMContentLoaded' , load , true );
                        document.removeEventListener( 'DomContentLoaded' , load , true );
                        document.removeEventListener( 'load' , load , true );
                    }
                    
                    global.removeEventListener( 'load' , load , true );
                }
            }
            
            if ( document ) {
                document.addEventListener( 'DOMContentLoaded' , load , true );
                document.addEventListener( 'DomContentLoaded' , load , true );
                document.addEventListener( 'load' , load , true );
            }
            
            global.addEventListener( 'load' , load , true );
            
            return stack;
        } )() );
        
        /**
         * @readonly
         * @function matchesSelector
         * @param {HTMLElement|Element|Node} e - element
         * @param {string} s - css selector
         * @return {!boolean}
         * */
        _defineProperty( global , 'matchesSelector' , ( function () {
            var document = getdocument( global ) ,
                match = null ,
                tmp;
            
            if ( document ) {
                tmp = document.createElement( 'div' );
                
                match = tmp[ 'matches' ] && 'matches' ||
                    tmp[ 'matchesSelector' ] && 'matchesSelector' ||
                    tmp[ 'webkitMatchesSelector' ] && 'webkitMatchesSelector' ||
                    tmp[ 'mozMatchesSelector' ] && 'mozMatchesSelector' ||
                    tmp[ 'oMatchesSelector' ] && 'oMatchesSelector' ||
                    tmp[ 'msMatchesSelector' ] && 'msMatchesSelector' || null;
            }
            
            return function ( e , s ) {
                if ( !match || !e[ match ] ) {
                    return false;
                }
                
                return e[ match ]( s );
            };
        } )() );
        
        /**
         * @readonly
         * @function nl2br
         * @param {string} str
         * @return {!string}
         * */
        _defineProperty( global , 'nl2br' , function ( str ) {
            if ( isstring( str ) ) {
                return str
                    .replace( /\n/g , "<br/>" )
                    .replace( /\t/g , "&nbsp;&nbsp;&nbsp;&nbsp;" );
            }
            
            return '';
        } );
        
        /**
         * @readonly
         * @function inarray
         * @param {*} _in
         * @param {Array} array
         * @return {!boolean}
         * */
        _defineProperty( global , 'inarray' , function ( _in , array ) {
            return array.indexOf( _in ) >= 0;
        } );
        
        /**
         * @readonly
         * @function arrayunique
         * @param {Array} _
         * @return {!Array}
         * */
        _defineProperty( global , 'arrayunique' , function ( _ ) {
            for ( var n = [] , i = 0 , l = _.length ; i < l ; i++ ) {
                if ( !inarray( _[ i ] , n ) ) {
                    n.push( _[ i ] );
                }
            }
            return n;
        } );
        
        /**
         * @readonly
         * @function typedtoarray
         * @param {*} arg
         * @return {!Array}
         * */
        _defineProperty( global , 'typedtoarray' , function ( arg ) {
            return Array.apply( null , arg );
        } );
        
        /**
         * @readonly
         * @function fragmenttoarray
         * @param {DocumentFragment} frag
         * @return {!Array}
         * */
        _defineProperty( global , 'fragmenttoarray' , function ( frag ) {
            return typedtoarray( frag.children );
        } );
        
        /**
         * @readonly
         * @function getallchildren
         * @param {HTMLElement|Element|Node} element
         * @return {!NodeList}
         * */
        _defineProperty( global , 'getallchildren' , function ( element ) {
            return element.getElementsByTagName( '*' );
        } );
        
        /**
         * @readonly
         * @function emptyNode
         * @param {HTMLElement|Element|Node} element
         * @return {!(HTMLElement|Element|Node)}
         * */
        _defineProperty( global , 'emptyNode' , function ( element ) {
            var c;
            
            while ( c = element.firstChild ) {
                element.removeChild( c );
            }
            
            return element;
        } );
        
        /**
         * @readonly
         * @function closest
         * @param {HTMLElement|Element|Node} element
         * @param {string} selector
         * @return {?(HTMLElement|Element|Node)}
         * */
        _defineProperty( global , 'closest' , ( function () {
            var document = getdocument( global );
            
            if ( document && isfunction( document.createElement( 'div' ).closest ) ) {
                return function ( element , selector ) {
                    if ( !isnode( element ) ) {
                        return null;
                    }
                    
                    return element.closest( selector );
                };
            }
            
            return function ( element , selector ) {
                if ( !isnode( element ) ) {
                    return null;
                }
                
                if ( matchesSelector( element , selector ) ) {
                    return element;
                }
                
                while ( isnode( element = element.parentNode ) ) {
                    if ( matchesSelector( element , selector ) ) {
                        return element;
                    }
                }
                
                return null;
            };
        } )() );
        
        /**
         * @readonly
         * @function randomstring
         * @return {!string}
         * */
        _defineProperty( global , 'randomstring' , function () {
            return '_' + Math.random().toString( 30 ).substring( 2 );
        } );
        
        /**
         * @readonly
         * @function between
         * @param {number} min
         * @param {number} max
         * @return {!number}
         * */
        _defineProperty( global , 'between' , function ( min , max ) {
            return Math.floor( Math.random() * ( max - min + 1 ) + min );
        } );
        
        /**
         * @readonly
         * @function round
         * @param {number} int
         * @param {number} after
         * @return {!number}
         * */
        _defineProperty( global , 'round' , function ( int , after ) {
            return parseFloat( int.toFixed( isinteger( after ) ? after : int.toString().length ) );
        } );
        
        /**
         * @readonly
         * @function wrapint
         * @param {number} int
         * @param {number} howmany
         * @return {!number}
         * */
        _defineProperty( global , 'wrapinteger' , function ( int , howmany ) {
            int = int.toString();
            
            while ( int.length < howmany ) {
                int = '0' + int;
            }
            
            return int;
        } );
        
        /**
         * @readonly
         * @function jsonparse
         * @param {string} str
         * @return {?Object}
         * */
        _defineProperty( global , 'jsonparse' , function ( str ) {
            var result = null ,
                masterKey;
            
            try {
                result = JSON.parse( str );
            } catch ( e ) {
                try {
                    masterKey = randomstring();
                    
                    global[ masterKey ] = null;
                    
                    eval( 'global[ masterKey ] = ' + str );
                    
                    if ( isobject( global[ masterKey ] ) ) {
                        result = global[ masterKey ] || null;
                    }
                } catch ( e ) {
                    result = null;
                } finally {
                    delete global[ masterKey ];
                }
            }
            
            return result;
        } );
        
        if ( !isfunction( global.setImmediate ) ) {
            ( function () {
                var message = 0 ,
                    register = {} ,
                    prefix = randomstring() + '.immediate';
                
                function getLocation ( location ) {
                    try {
                        if ( isfillstring( location.origin ) && location.origin !== 'null' ) {
                            return location.origin;
                        }
                        
                        if ( location.ancestorOrigins && location.ancestorOrigins.length ) {
                            return location.ancestorOrigins[ 0 ];
                        }
                    } catch ( _ ) {
                        return null;
                    }
                }
                
                var origin = ( function () {
                    var init = global;
                    
                    while ( true ) {
                        var tmp = getLocation( init.location );
                        
                        if ( tmp ) {
                            return tmp;
                        }
                        
                        if ( iswindow( init.parent ) ) {
                            init = init.parent;
                            continue;
                        }
                        
                        return '';
                    }
                } )();
                
                try {
                    global.postMessage( prefix + 1 , function () {} );
                } catch ( e ) {
                    return ( function () {
                        _defineProperty( global , 'setImmediate' , global.setTimeout );
                        _defineProperty( global , 'clearImmediate' , global.clearTimeout );
                    } )();
                }
                
                global.addEventListener( 'message' , function ( event ) {
                    if ( origin === event.origin ) {
                        var i = event.data;
                        
                        if ( isfunction( register[ i ] ) ) {
                            register[ i ]();
                            delete register[ i ];
                            event.stopImmediatePropagation();
                        }
                    }
                } , eventPassiveSupport ? { capture : true , passive : true } : true );
                
                /**
                 * @readonly
                 * @function setImmediate
                 * @param {function} fn
                 * @return {!number}
                 * */
                _defineProperty( global , 'setImmediate' , function ( fn ) {
                    var id = ++message;
                    
                    register[ prefix + id ] = fn;
                    global.postMessage( prefix + id , origin );
                    
                    return id;
                } );
                
                /**
                 * @readonly
                 * @function clearImmediate
                 * @param {number} id
                 * @return void
                 * */
                _defineProperty( global , 'clearImmediate' , function ( id ) {
                    if ( register[ prefix + id ] ) {
                        delete register[ prefix + id ];
                    }
                } );
            } )();
        }
        
        /**
         * @readonly
         * @function getstyleproperty
         * @param {HTMLElement|Element|Node} element
         * @param {string} property
         * @return {?string}
         * */
        _defineProperty( global , 'getstyleproperty' , function ( element , property ) {
            var val , win;
            
            if ( isnode( element ) ) {
                
                if ( val = element.style[ property ] ) {
                    return val;
                }
                
                if ( val = element.style.getPropertyValue( property ) ) {
                    return val;
                }
                
                if ( win = getwindow( element ) ) {
                    return win.getComputedStyle( element ).getPropertyValue( property ) || null;
                }
                
            }
            
            return null;
        } );
        
        /**
         * @readonly
         * @function splitTrim
         * @param {string} str
         * @param {RegExp|string} spl
         * @return {Array}
         * */
        _defineProperty( global , 'splitTrim' , function ( str , spl ) {
            var ar = str.split( spl );
            
            for ( var i = 0 , r = [] , tmp ; i < ar.length ; ) {
                if ( tmp = ar[ i++ ].trim() ) {
                    r.push( tmp );
                }
            }
            
            return r;
        } );
        
        /**
         * @readonly
         * @function clonearray
         * @param {Array} _
         * @return {!Array}
         * */
        _defineProperty( global , 'clonearray' , function ( _ ) {
            return _.slice( 0 );
        } );
        
        /**
         * @readonly
         * @function toarray
         * @param {*} _
         * @return {!Array}
         * */
        _defineProperty( global , 'toarray' , function ( _ ) {
            return isset( _ ) ? ( isarray( _ ) ? _ : [ _ ] ) : [];
        } );
        
        /**
         * @readonly
         * @function counterCallback
         * @param {number} counter
         * @param {function} callback
         * @return {!Array}
         * */
        _defineProperty( global , 'counterCallback' , function ( counter , callback ) {
            var current = 0;
            
            return function () {
                if ( ++current == counter ) {
                    callback();
                }
            };
        } );
        
        /**
         * @readonly
         * @function regexpEscapeString
         * @param {string} str
         * @return {!string}
         * */
        _defineProperty( global , 'regexpEscapeString' , function ( str ) {
            if ( exist( str ) ) {
                return str.toString()
                    .replace( /\\/gi , '\\\\' )
                    .replace( /([.\[\](){}^?*|+\-$])/gi , '\\$1' );
            }
            
            return '';
        } );
        
        /**
         * @readonly
         * @function htmlEscapeString
         * @param {string} str
         * @return {!string}
         * */
        _defineProperty( global , 'htmlEscapeString' , function ( str ) {
            if ( exist( str ) ) {
                return str.toString()
                    .replace( /&/gi , '&amp;' )
                    .replace( /"/gi , '&quot;' )
                    .replace( /'/gi , '&apos;' )
                    .replace( /</gi , '&lt;' )
                    .replace( />/gi , '&gt;' );
            }
            
            return '';
        } );
        
        /**
         * @readonly
         * @function resetregexp
         * @param {RegExp} _
         * @return {?RegExp}
         * */
        _defineProperty( global , 'resetregexp' , function ( _ ) {
            if ( !isregexp( _ ) ) {
                return null;
            }
            
            return _.lastIndex = _.index = 0, _;
        } );
        
        /**
         * @readonly
         * @function overrideObject
         * @return {!Object}
         * */
        _defineProperty( global , 'overrideObject' , function () {
            var result = {} ,
                array = typedtoarray( arguments );
            
            if ( array.length <= 0 ) {
                return result;
            }
            
            if ( array.length == 1 ) {
                return isobject( array[ 0 ] ) ? array[ 0 ] : result;
            }
            
            for ( var i = 0 , l = array.length ; i < l ; i++ ) {
                forin( array[ i ] , function ( key , value ) {
                    result[ key ] = value;
                } );
            }
            
            return result;
        } );
        
        /**
         * @readonly
         * @function isEmptyObject
         * @param {Object} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isEmptyObject' , function ( _ ) {
            if ( isobject( _ ) ) {
                for ( var key in _ ) {
                    if ( hasProperty.call( _ , key ) ) {
                        return false;
                    }
                }
                
                return true;
            }
            
            return false;
        } );
        
        /**
         * @readonly
         * @function forin
         * @param {Object} obj
         * @param {function} callback
         * @return void
         * */
        _defineProperty( global , 'forin' , function ( obj , callback ) {
            var key , value;
            
            if ( isobject( obj ) ) {
                for ( key in obj ) {
                    if ( hasProperty.call( obj , key ) && isset( value = obj[ key ] ) ) {
                        callback( key , value , obj );
                    }
                }
            }
        } );
        
        /**
         * @readonly
         * @function foreach
         * @param {Array} arr
         * @param {function} callback
         * @return void
         * */
        _defineProperty( global , 'foreach' , function ( arr , callback ) {
            if ( isarray( arr ) ) {
                for ( var i = 0 , l = arr.length ; i < l ; i++ ) {
                    callback( arr[ i ] , i , arr );
                }
            }
        } );
        
        /**
         * @readonly
         * @function for
         * @param {Array} arg
         * @param {function} callback
         * @return void
         * */
        _defineProperty( global , 'for' , function ( arg , callback ) {
            if ( isarray( arg ) ) {
                foreach( arg , callback );
            }
            else if ( isobject( arg ) ) {
                forin( arg , callback );
            }
        } );
        
        /**
         * @readonly
         * @function smoothyIncrement
         * @param {Object} setting
         * @param {number} setting.begin
         * @param {number} setting.end
         * @param {function} setting.eachFrame
         * @param {function} [setting.onFinish]
         * @param {number} [setting.speed=500]
         * @return void
         * */
        _defineProperty( global , 'smoothyIncrement' , ( function () {
            
            var frameLatency = 1000 / 60;
            
            var requestFrame = global[ 'requestAnimationFrame' ] ||
                global[ 'webkitRequestAnimationFrame' ] ||
                global[ 'mozRequestAnimationFrame' ] ||
                global[ 'msRequestAnimationFrame' ];
            
            var cancelFrame = global[ 'cancelAnimationFrame' ] ||
                global[ 'webkitCancelAnimationFrame' ] ||
                global[ 'mozCancelAnimationFrame' ] ||
                global[ 'msCancelAnimationFrame' ];
            
            function ease ( n ) {
                return 0.5 * ( 1 - Math.cos( Math.PI * n ) );
            }
            
            function animate ( callframe ) {
                var frame , start;
                
                function loop () {
                    frame = requestFrame( loop );
                    callframe();
                }
                
                start = setImmediate( loop );
                
                return function () {
                    clearImmediate( start );
                    cancelFrame( frame );
                };
            }
            
            return function ( setting ) {
                
                setting = overrideObject( {
                    speed : 1000
                } , setting );
                
                var begin = setting.begin ,
                    end = setting.end ,
                    speed = setting.speed ,
                    callback = setting.eachFrame ,
                    callbackFinish = setting.onFinish;
                
                if ( !isinteger( speed ) || speed <= 0 ) {
                    speed = 1000;
                }
                
                speed = Math.round( speed / 1.8 );
                
                var tmp = begin;
                begin = Math.min( begin , end );
                end = Math.max( tmp , end );
                
                var newval , elapsed;
                
                var firstStep = true ,
                    lastStep = false ,
                    endAtNext = false;
                
                var beginValues = [] ,
                    beginTotal = 0 ,
                    total = 0;
                
                var elapsedMax = 1 ,
                    elapsedMin = 0.1;
                
                var totalScroll = end - begin ,
                    increment = Math.round( totalScroll / ( speed / frameLatency ) );
                
                if ( totalScroll <= increment ) {
                    return callback( end , end - begin );
                }
                
                var startTime = Date.now();
                
                var stop = animate( function () {
                    
                    /* stop process */
                    /* ---------------------- */
                    if ( endAtNext ) {
                        callback( end , end - begin );
                        
                        if ( isfunction( callbackFinish ) ) {
                            callbackFinish();
                        }
                        
                        return stop();
                    }
                    
                    /* calc process */
                    /* ---------------------- */
                    
                    elapsed = round( ( Date.now() - startTime ) / speed , 2 );
                    
                    if ( elapsed < elapsedMin ) {
                        elapsed = elapsedMin;
                    }
                    else if ( elapsed > elapsedMax ) {
                        elapsed = elapsedMax;
                    }
                    
                    if ( firstStep && elapsed == elapsedMax ) {
                        firstStep = false;
                    }
                    
                    /* begin process */
                    /* ---------------------- */
                    
                    newval = Math.round( increment * ease( elapsed ) );
                    
                    if ( newval < 1 ) {
                        newval = 1;
                    }
                    
                    /* middle process */
                    /* ---------------------- */
                    
                    if ( firstStep ) {
                        beginValues.push( newval );
                        beginTotal += newval;
                    }
                    
                    if ( !firstStep && !lastStep && ( totalScroll - total ) <= beginTotal ) {
                        lastStep = true;
                    }
                    
                    /* end process */
                    /* ---------------------- */
                    
                    if ( lastStep ) {
                        if ( beginValues.length ) {
                            newval = beginValues.pop();
                        }
                        else {
                            newval = 1;
                        }
                    }
                    
                    total += newval;
                    
                    /* request stop process */
                    /* ---------------------- */
                    
                    if ( lastStep && totalScroll - total <= newval ) {
                        endAtNext = true;
                    }
                    
                    /* callback */
                    /* ---------------------- */
                    
                    callback( begin += newval , newval );
                    
                } );
                
                return stop;
                
            };
            
        } )() );
        
        /**
         * @readonly
         * @function cacheHandler
         * @param {number} [n=500]
         * @return {!function}
         * */
        _defineProperty( global , 'cacheHandler' , function ( n ) {
            var c = [];
            !n && ( n = 500 );
            
            function SAVE ( k , v ) {
                if ( !/^_(rm|purge)$/g.test( k ) ) {
                    SAVE[ k ] = v;
                    c.push( k );
                    
                    if ( c.length > n ) {
                        delete SAVE[ c.shift() ];
                    }
                }
            }
            
            SAVE._rm = function ( k ) {
                var i;
                
                if ( ( i = c.indexOf( k ), i ) !== -1 ) {
                    delete SAVE[ k ];
                    c.splice( i , 1 );
                }
            };
            
            SAVE._purge = function () {
                var i = c.length;
                
                while ( i-- ) {
                    delete SAVE[ c.shift() ];
                }
            };
            
            return SAVE;
        } );
        
        /**
         * @readonly
         * @function requireJS
         * @param {string|Array} source
         * @param {function} callback
         * @param {Document} [document=Document]
         * @return void
         * */
        _defineProperty( global , 'requireJS' , ( function () {
            var doc = getdocument( global );
            
            function require ( src , callback , document ) {
                var script;
                
                
                function load ( e ) {
                    callback();
                    e && e.type == 'load' && script.removeEventListener( 'load' , load );
                }
                
                if ( document ) {
                    
                    if ( document.querySelector( 'script[src="' + src + '"]' ) ) {
                        return load();
                    }
                    
                    script = document.createElement( 'script' );
                    
                    script.setAttribute( 'src' , src );
                    script.setAttribute( 'async' , 'async' );
                    script.setAttribute( 'type' , 'text/javascript' );
                    
                    script.addEventListener( 'load' , load );
                    
                    document.head.appendChild( script );
                    
                }
            }
            
            return function ( source , callback , document ) {
                var scripts = toarray( source ) ,
                    l = scripts.length ,
                    i = 0;
                
                function loop () {
                    if ( i < l ) {
                        return require( scripts[ i++ ] , loop , document || doc );
                    }
                    
                    if ( isfunction( callback ) ) {
                        callback();
                    }
                }
                
                loop();
            };
            
        } )() );
        
        /**
         * @readonly
         * @function requireCSS
         * @param {string|Array} source
         * @param {function} callback
         * @param {Document} [document=Document]
         * @return void
         * */
        _defineProperty( global , 'requireCSS' , ( function () {
            var doc = getdocument( global );
            
            function require ( href , callback , document ) {
                var link;
                
                function load ( e ) {
                    callback();
                    e && e.type == 'load' && link.removeEventListener( 'load' , load );
                }
                
                if ( document ) {
                    
                    if ( document.querySelector( 'link[href="' + href + '"]' ) ) {
                        return load();
                    }
                    
                    link = document.createElement( 'link' );
                    
                    link.setAttribute( 'href' , href );
                    link.setAttribute( 'rel' , 'stylesheet' );
                    
                    link.addEventListener( 'load' , load );
                    
                    document.head.appendChild( link );
                    
                }
            }
            
            return function ( source , callback , document ) {
                var scripts = toarray( source ) ,
                    l = scripts.length ,
                    i = 0;
                
                function loop () {
                    if ( i < l ) {
                        return require( scripts[ i++ ] , loop , document || doc );
                    }
                    
                    if ( isfunction( callback ) ) {
                        callback();
                    }
                }
                
                loop();
            };
            
        } )() );
        
        /**
         * @readonly
         * @function htmlDOM
         * @param {string} str
         * @return {?(HTMLElement|Element|Node|array)}
         * */
        _defineProperty( global , 'htmlDOM' , ( function () {
            var document = getdocument( global );
            
            if ( !document ) {
                return function () {
                    return null;
                };
            }
            
            var parentOf = {
                "col" : "colgroup" ,
                "tr" : "tbody" ,
                "th" : "tr" ,
                "td" : "tr" ,
                "colgroup" : "table" ,
                "tbody" : "table" ,
                "thead" : "table" ,
                "tfoot" : "table" ,
                "dt" : "dl" ,
                "dd" : "dl" ,
                "figcaption" : "figure" ,
                "legend" : "fieldset" ,
                "fieldset" : "form" ,
                "keygen" : "form" ,
                "area" : "map" ,
                "menuitem" : "menu" ,
                "li" : "ul" ,
                "option" : "optgroup" ,
                "optgroup" : "select" ,
                "output" : "form" ,
                "rt" : "ruby" ,
                "rp" : "ruby" ,
                "summary" : "details" ,
                "track" : "video" ,
                "source" : "video" ,
                "param" : "object"
            };
            
            var _ = cacheHandler();
            
            function clone ( e ) {
                if ( isarray( e ) ) {
                    for ( var i = 0 , l = e.length , r = [] ; i < l ; i++ ) {
                        r.push( e[ i ].cloneNode( true ) );
                    }
                    return r;
                }
                
                if ( exist( e ) ) {
                    return e.cloneNode( true );
                }
                
                return null;
            }
            
            function child ( e ) {
                return e.children.length == 1 ? e.children[ 0 ] : Array.apply( null , e.children );
            }
            
            function createHTML ( str ) {
                var firstTag = ( /^(<[\s]*[\w]{1,15}[\s]*(\b|>)?)/gi.exec( str ) || [ '' ] )[ 0 ]
                    .replace( /\W/g , '' ).trim().toLowerCase() ,
                    parent , doc;
                
                if ( !firstTag ) {
                    return;
                }
                
                if ( !parentOf[ firstTag ] ) {
                    doc = document.createDocumentFragment();
                    parent = document.createElement( 'DIV' );
                    doc.appendChild( parent );
                    parent.innerHTML = str;
                    return child( parent );
                }
                
                /** @type {Node} */
                parent = createHTML( '<' + parentOf[ firstTag ] + '></' + parentOf[ firstTag ] + '>' );
                
                while ( parent.firstChild ) {
                    parent = parent.firstChild;
                }
                
                parent.innerHTML = str;
                
                return child( parent );
            }
            
            return function ( str ) {
                var r;
                
                str = str.trim();
                
                if ( _[ str ] ) {
                    return clone( _[ str ] );
                }
                
                if ( !/(<[\s]*(\/)?[\s]*[\w]{1,15}[\s]*(\/)?[\s]*(\b|>)?)/gi.test( str ) ) {
                    return null;
                }
                
                if ( /^(<\/[\s]*[\w]{1,15}[\s]*>)$/gi.test( str ) ) {
                    r = document.createElement( str.replace( /\W/g , '' ).toUpperCase() );
                }
                else {
                    r = createHTML( str );
                }
                
                return _( str , r ) , clone( r );
            };
        } )() );
        
    } )();
    
    var _tmatch ,
        _tclosest ,
        level = 0 ,
        concat = Array.prototype.push;
    
    // cache
    var lvl = cacheHandler() ,
        findCache = cacheHandler() ,
        multiCache = cacheHandler();
    
    function getHtml ( e ) {
        return getdocument( e ).documentElement;
    }
    
    // return "ar2" items - "ar" items
    function filterDiff ( ar , ar2 ) {
        var r = [];
        
        if ( ar.length ) {
            for ( var i = 0 , l = ar2.length ; i < l ; i++ ) {
                if ( !inarray( ar2[ i ] , ar ) ) {
                    r.push( ar2[ i ] );
                }
            }
        }
        
        return r;
    }
    
    function nodelistFilter ( nodelist , filter , reverse ) {
        for ( var i = 0 , l = nodelist.length , r = [] , node ; i < l ; i++ ) {
            node = nodelist[ i ];
            
            if ( _tmatch( node , filter , node.parentNode ) ) {
                r.push( node );
            }
        }
        
        return reverse ? r.reverse() : r;
    }
    
    function globalNodelistFilter ( nodelist , filter ) {
        for ( var i = 0 , l = nodelist.length , r = [] , node ; i < l ; i++ ) {
            node = nodelist[ i ];
            
            if ( _tmatch( node , filter ) ) {
                r.push( node );
            }
        }
        
        return r;
    }
    
    var applyToolsOnNodelist = ( function () {
        var first = /^([*.:"'{})(=~|^$+#>a-z0-9\[\]-]+)(\b)?/gi;
        
        function firstLevel ( selector ) {
            var r = lvl[ selector ];
            
            if ( r ) {
                return r;
            }
            
            r = resetregexp( first ).exec( selector )[ 0 ];
            lvl( selector , r );
            
            return r;
        }
        
        return function ( nodelist , filter ) {
            var i = 0 , l = nodelist.length , n , p , r = [] ,
                compiled = compiler( filter , '*' , filter + '*' )[ 0 ][ 0 ] ,
                s = firstLevel( compiled[ 0 ] );
            
            for ( i = 0 ; i < l ; i++ ) {
                n = nodelist[ i ] , p = n.parentNode;
                
                if ( _tmatch( n , firstLevel( s ) , p ) ) {
                    concat.apply( r , advanceRequest( {
                        container : p ,
                        nodelist : [ n ] ,
                        tool : compiled[ 1 ] ,
                        selector : compiled[ 0 ]
                    } ) );
                }
            }
            
            return r;
        };
    } )();
    
    var animated = [];
    ( function ( event , listenerArgument ) {
        if ( event && isfunction( global.addEventListener ) ) {
            
            event.slice( 0 , 2 ).forEach( function ( e ) {
                global.addEventListener( e , function ( e ) {
                    animated.push( e.target );
                } , listenerArgument );
            } );
            
            event.slice( 2 ).forEach( function ( e ) {
                global.addEventListener( e , function ( e ) {
                    var i;
                    if ( ( i = animated.indexOf( e.target ) , i ) !== -1 ) {
                        animated.splice( i , 1 );
                    }
                } , listenerArgument );
            } );
            
        }
    } )(
        // animation events
        ( function () {
            var test = document.createElement( 'DIV' ).style ,
                
                pre = isset( test[ 'animation' ] ) && [
                        'animationstart' , 'transitionstart' , 'animationend' , 'transitionend '
                    ] ||
                    ( isset( test[ 'oAnimation' ] ) || isset( test[ 'OAnimation' ] ) ) && [
                        'oanimationstart' , 'otransitionstart' , 'oanimationend' , ' otransitionend'
                    ] ||
                    isset( test[ 'webkitAnimation' ] ) && [
                        'webkitAnimationstart' , 'webkitTransitionstart' , 'webkitAnimationEnd' , 'webkitTransitionEnd'
                    ] || null;
            
            return test = null, pre;
        } )() ,
        
        // passive event support
        eventPassiveSupport ? { capture : true , passive : true } : true
    );
    
    function childrenNodes ( element , r ) {
        var c = typedtoarray( element.children );
        return r ? c.reverse() : c;
    }
    
    var onlyChildsOf = ( function () {
        
        function isParent ( e , p ) {
            return p !== e && p.contains( e );
        }
        
        return function ( parent , nodelist ) {
            for ( var i = 0 , l = nodelist.length , r = [] , tmp ; i < l ; i++ ) {
                if ( tmp = nodelist[ i ] , isParent( tmp , parent ) ) {
                    r.push( tmp );
                }
            }
            return r;
        };
    } )();
    
    var animationState = ( function () {
        var animationStateProp = ( function () {
            var test = document.createElement( 'DIV' ).style ,
                pre = isset( test[ 'animationPlayState' ] ) ? '' :
                    isset( test[ 'OAnimationPlayState' ] ) ? '-o-' :
                        isset( test[ 'MozAnimationPlayState' ] ) ? '-moz-' :
                            isset( test[ 'WebkitAnimationPlayState' ] ) ? '-webkit-' : '';
            return test = null , pre;
        } )() + 'animation-play-state';
        
        return function ( node ) {
            return getstyleproperty( node , animationStateProp );
        };
    } )();
    
    // find index of f in s
    // ex f = ","
    // "div , p" return [5]
    // but "div:not(a,p)" return [0]
    function multiIndexOf ( f , s ) {
        var open , p , i , c , tmp = f + '_' + s;
        
        if ( multiCache[ tmp ] ) {
            return multiCache[ tmp ];
        }
        
        for ( open = 0, p = [], i = 0 ; c = s.charAt( i ) ; i++ ) {
            if ( /([(\[])/g.exec( c ) ) {
                open++;
            }
            else if ( /([)\]])/g.exec( c ) ) {
                open--;
            }
            else if ( c === f && !open ) {
                p.push( i );
            }
        }
        
        return multiCache( tmp , p.reverse() ), multiCache[ tmp ];
    }
    
    // like multiIndexOf but return only first index of "(" ( or null )
    function firstIndexOf ( f , s ) {
        for ( var open = 0 , p = null , i = 0 , c ; c = s.charAt( i ) ; i++ ) {
            if ( /([(\[])/g.exec( c ) ) {
                open++;
            }
            else if ( /([)\]])/g.exec( c ) ) {
                open--;
            }
            else if ( c === f && !open ) {
                p = i;
                break;
            }
        }
        
        return p;
    }
    
    // like multiIndexOf but return last index of ")" ( or null )
    function findCloser ( s ) {
        for ( var open = 0 , p = null , i = 0 , c ; c = s.charAt( i ) ; i++ ) {
            if ( /([(\[])/g.exec( c ) ) {
                open++;
            }
            else if ( /([)\]])/g.exec( c ) ) {
                if ( !--open && c === ')' ) {
                    p = i;
                    break;
                }
            }
        }
        
        return p;
    }
    
    // check if a css tool is natively supported
    // ex :
    // support('checked') can be true
    // support('nth-of-type') too
    // support('yolo') return false...
    function support ( flag , b ) {
        try {
            return document.querySelector( ':' + flag ), true;
        } catch ( _ ) {
            return b ? false : support( flag + '(0)' , true );
        }
    }
    
    // css selector
    // use getElementById / getElementsByClassName / getElementsByTagName if it's possible, else querySelectorAll
    var query = ( function () {
        var qsa = function ( s , c ) {
                return c.querySelectorAll( s );
            } ,
            by = {
                "#" : function ( i ) {
                    return document.getElementById( i );
                } ,
                "." : document.getElementsByClassName ? function ( i , c ) {
                    return c.getElementsByClassName( i );
                } : function ( i , c ) {
                    return qsa( '.' + i , c );
                }
            } ,
            hard = /[\s:.)\[><~^+]+/g;
        
        function get ( s , c ) {
            var t = s.charAt( 0 ) , l = s.slice( 1 ) , b = ( t == '.' ) , tmp;
            
            if ( ( b && resetregexp( hard ).exec( l ) ) || ( !b && resetregexp( hard ).exec( s ) ) ) {
                return qsa( s , c );
            }
            
            if ( tmp = by[ t ] ) {
                return tmp( l , c );
            }
            
            return c.getElementsByTagName( s );
        }
        
        return function ( s , c ) {
            
            switch ( s ) {
                
                case 'document':
                    return [ global.document ];
                
                case 'window':
                    return [ global ];
                
            }
            
            var tmp = get( s , c );
            
            if ( exist( tmp ) ) {
                
                if ( exist( tmp.length ) ) {
                    return typedtoarray( tmp );
                }
                
                return [ tmp ];
            }
            
            return [];
        };
    } )();
    
    // targetJS features
    var advSelector = {
        
        'root' : function ( n , f , s ) {
            return [ getHtml( s.container ) ];
        } ,
        
        "before-all" : function ( nodelist , flag ) {
            if ( !flag ) {
                return [];
            }
            
            for ( var i = 0 , l = nodelist.length , r = [] , node , next ; i < l ; i++ ) {
                node = nodelist[ i ];
                next = node.nextElementSibling;
                
                while ( next ) {
                    if ( _tmatch( next , flag ) ) {
                        r.push( node );
                        break;
                    }
                    next = next.nextElementSibling;
                }
            }
            
            return r;
        } ,
        
        "after-all" : function ( nodelist , flag ) {
            if ( !flag ) {
                return [];
            }
            
            for ( var i = 0 , l = nodelist.length , r = [] , node , next ; i < l ; i++ ) {
                node = nodelist[ i ];
                next = node.previousElementSibling;
                
                while ( next ) {
                    if ( _tmatch( next , flag ) ) {
                        r.push( node );
                        break;
                    }
                    next = next.previousElementSibling;
                }
            }
            
            return r;
        } ,
        
        "before" : function ( nodelist , flag ) {
            if ( !flag ) {
                return [];
            }
            
            for ( var i = 0 , l = nodelist.length , r = [] , node ; i < l ; i++ ) {
                node = nodelist[ i ];
                if ( _tmatch( node.nextElementSibling , flag ) ) {
                    r.push( node );
                }
            }
            
            return r;
        } ,
        
        "after" : function ( nodelist , flag ) {
            if ( !flag ) {
                return [];
            }
            
            for ( var i = 0 , l = nodelist.length , r = [] , node ; i < l ; i++ ) {
                node = nodelist[ i ];
                if ( _tmatch( node.previousElementSibling , flag ) ) {
                    r.push( node );
                }
            }
            
            return r;
        } ,
        
        'animated' : function ( nodelist ) {
            for ( var i = 0 , l = nodelist.length , r = [] ; i < l ; i++ ) {
                if ( inarray( nodelist[ i ] , animated ) && animationState( nodelist[ i ] ) !== "paused" ) {
                    r.push( nodelist[ i ] );
                }
            }
            return r;
        } ,
        
        'contains-cs' : function ( nodelist , flag ) {
            if ( !flag ) {
                return [];
            }
            
            var reg = new RegExp( '(' + regexpEscapeString( flag ) + ')' , 'gi' );
            for ( var i = 0 , l = nodelist.length , r = [] ; i < l ; i++ ) {
                if ( resetregexp( reg ).exec( nodelist[ i ].textContent || nodelist[ i ].innerText ) ) {
                    r.push( nodelist[ i ] );
                }
            }
            
            return r;
        } ,
        
        'contains' : function ( nodelist , flag ) {
            if ( !flag ) {
                return [];
            }
            
            var reg = new RegExp( '(' + regexpEscapeString( flag ) + ')' , 'g' );
            for ( var i = 0 , l = nodelist.length , r = [] ; i < l ; i++ ) {
                if ( resetregexp( reg ).exec( nodelist[ i ].textContent || nodelist[ i ].innerText ) ) {
                    r.push( nodelist[ i ] );
                }
            }
            
            return r;
        } ,
        
        'first' : function ( nodelist , n , obj ) {
            return obj.selector == '*' ? nodelistFilter( nodelist , '' ) :
                nodelist.length ? [ nodelist[ 0 ] ] : [];
        } ,
        
        'last' : function ( nodelist , n , obj ) {
            return obj.selector == '*' ? nodelistFilter( nodelist , ':last-child' ) :
                nodelist.length ? [ nodelist[ nodelist.length - 1 ] ] : [];
        } ,
        
        'is-parent' : function ( nodelist ) {
            for ( var i = 0 , r = [] , l = nodelist.length ; i < l ; i++ ) {
                if ( childrenNodes( nodelist[ i ] ).length ) {
                    r.push( nodelist[ i ] );
                }
            }
            return r;
        } ,
        
        'is-child-of' : function ( nodelist , flag ) {
            if ( !flag ) {
                return [];
            }
            
            for ( var i = 0 , r = [] , l = nodelist.length , node ; i < l ; i++ ) {
                if ( isnode( node = nodelist[ i ].parentNode ) && _tmatch( node , flag ) ) {
                    r.push( nodelist[ i ] );
                }
            }
            
            return r;
        } ,
        
        'draggable' : function ( nodelist ) {
            for ( var i = 0 , l = nodelist.length , r = [] ; i < l ; i++ ) {
                if ( nodelist[ i ].draggable ) {
                    r.push( nodelist[ i ] );
                }
            }
            return r;
        } ,
        
        'is' : function ( nodelist , flag ) {
            if ( !flag ) {
                return [];
            }
            
            return nodelistFilter( nodelist , flag );
        } ,
        
        'has' : function ( nodelist , flag ) {
            if ( !flag ) {
                return [];
            }
            
            for ( var i = 0 , r = [] , l = nodelist.length , f = find ; i < l ; i++ ) {
                if ( !!f( flag , nodelist[ i ] ).length ) {
                    r.push( nodelist[ i ] );
                }
            }
            
            return r;
        } ,
        
        'find' : function ( nodelist , flag ) {
            if ( !flag ) {
                return [];
            }
            
            for ( var i = 0 , r = [] , l = nodelist.length ; i < l ; ) {
                concat.apply( r , find( flag , nodelist[ i++ ] ) );
            }
            
            return r;
        } ,
        
        'closest' : function ( nodelist , flag ) {
            for ( var i = 0 , l = nodelist.length , r = [] , node , tmp ; i < l ; i++ ) {
                if ( !isnode( node = nodelist[ i ] , node ).parentNode ) {
                    continue;
                }
                
                if ( flag ) {
                    if ( tmp = _tclosest( node.parentNode , flag ) ) {
                        r.push( tmp );
                    }
                    continue;
                }
                
                while ( isnode( node = node.parentNode ) ) {
                    r.push( node );
                }
            }
            return r;
        } ,
        
        'parent' : function ( nodelist , flag ) {
            for ( var i = 0 , l = nodelist.length , r = [] , node ; i < l ; i++ ) {
                if ( isnode( node = nodelist[ i ].parentNode ) &&
                    ( !flag || ( flag && _tmatch( node , flag ) ) ) ) {
                    r.push( node );
                }
            }
            return r;
        } ,
        
        'value' : function ( nodelist ) {
            for ( var i = 0 , l = nodelist.length , r = [] , node ; i < l ; i++ ) {
                if ( ( isset( ( node = nodelist[ i ] ).value ) && !!node.value.toString().trim() ) ) {
                    r.push( node );
                }
            }
            return r;
        } ,
        
        'no-value' : function ( nodelist ) {
            for ( var i = 0 , l = nodelist.length , r = [] , node ; i < l ; i++ ) {
                if ( ( isset( ( node = nodelist[ i ] ).value ) && !node.value.toString().trim() ) ) {
                    r.push( node );
                }
            }
            return r;
        } ,
        
        'unchecked' : function ( nodelist ) {
            for ( var i = 0 , l = nodelist.length , r = [] , t , n ; i < l ; i++ ) {
                t = nodelist[ i ], n = t.tagName.toLowerCase();
                
                if ( ( n == 'input' && !t.checked ) || ( n == 'option' && !t.selected ) ) {
                    r.push( t );
                }
            }
            return r;
        } ,
        
        'selected' : function ( nodelist ) {
            for ( var i = 0 , l = nodelist.length , r = [] , t ; i < l ; i++ ) {
                t = nodelist[ i ];
                
                if ( t.tagName.toLowerCase() == 'option' && t.selected ) {
                    r.push( t );
                }
            }
            return r;
        } ,
        
        'unselected' : function ( nodelist ) {
            for ( var i = 0 , l = nodelist.length , r = [] , t ; i < l ; i++ ) {
                t = nodelist[ i ];
                
                if ( t.tagName.toLowerCase() == 'option' && !t.selected ) {
                    r.push( t );
                }
            }
            return r;
        } ,
        
        'blur' : function ( nodelist , flag , s ) {
            var a = getdocument( s.container ).activeElement;
            return filterDiff( a ? [ a ] : query( ':focus' , s.container ) , nodelist );
        } ,
        
        'not' : function ( nodelist , flag , setting ) {
            if ( !flag ) {
                return [];
            }
            return filterDiff(
                !/[:(\[><~+]/gi.exec( flag ) ?
                    query( flag , setting.container ) :
                    ( find( flag , setting.container ) || [] )
                , nodelist
            );
        } ,
        
        'next' : function ( nodelist , flag , s ) {
            for ( var node , i = 0 , l = nodelist.length , r = [] , html = getHtml( s.container ) ; i < l ; i++ ) {
                node = nodelist[ i ].nextElementSibling;
                
                if ( node && ( !flag || ( flag && _tmatch( node , flag , node.parentNode || html ) ) ) ) {
                    r.push( node );
                }
            }
            return r;
        } ,
        
        'previous' : function ( nodelist , flag , s ) {
            for ( var node , i = 0 , l = nodelist.length , r = [] , html = getHtml( s.container ) ; i < l ; i++ ) {
                node = nodelist[ i ].previousElementSibling;
                
                if ( node && ( !flag || ( flag && _tmatch( node , flag , node.parentNode || html ) ) ) ) {
                    r.push( node );
                }
            }
            return r;
        } ,
        
        'next-all' : function ( nodelist , flag , s ) {
            for ( var node , i = 0 , l = nodelist.length , r = [] , html = getHtml( s.container ) , tmp ; i < l ; i++ ) {
                node = nodelist[ i ];
                
                if ( flag ) {
                    tmp = [];
                    while ( node = node.nextElementSibling ) {
                        if ( !flag || ( flag && _tmatch( node , flag , node.parentNode || html ) ) ) {
                            tmp.push( node );
                        }
                    }
                    concat.apply( r , tmp );
                }
                else {
                    tmp = childrenNodes( node.parentNode );
                    concat.apply( r , tmp.slice( tmp.indexOf( node ) + 1 ) );
                }
            }
            return r;
        } ,
        
        'previous-all' : function ( nodelist , flag , s ) {
            for ( var node , i = 0 , l = nodelist.length , r = [] , html = getHtml( s.container ) , tmp ; i < l ; i++ ) {
                node = nodelist[ i ];
                
                if ( flag ) {
                    tmp = [];
                    while ( node = node.previousElementSibling ) {
                        if ( _tmatch( node , flag , node.parentNode || html ) ) {
                            tmp.push( node );
                        }
                    }
                    concat.apply( r , tmp.reverse() );
                }
                else {
                    tmp = childrenNodes( node.parentNode );
                    concat.apply( r , tmp.slice( 0 , tmp.indexOf( node ) ) );
                }
            }
            return r;
        } ,
        
        'children' : function ( nodelist , flag ) {
            for ( var i = 0 , l = nodelist.length , r = [] , node ; i < l ; i++ ) {
                if ( ( node = nodelist[ i ] ).firstChild ) {
                    concat.apply( r , flag ? nodelistFilter( childrenNodes( node ) , flag , true ) : childrenNodes( node ) );
                }
            }
            return r;
        } ,
        
        // for library only
        // -------------------------------------------------
        
        "fullAttr" : function ( nodelist , flag ) {
            for ( var i = 0 , l = nodelist.length , r = [] , node ; i < l ; i++ ) {
                node = nodelist[ i ];
                
                if ( node.hasAttribute( flag ) && !!( node.getAttribute( flag ) || '' ).trim() ) {
                    r.push( node );
                }
            }
            return r;
        } ,
        
        "emptyAttr" : function ( nodelist , flag ) {
            for ( var i = 0 , l = nodelist.length , r = [] , node ; i < l ; i++ ) {
                node = nodelist[ i ];
                
                if ( node.hasAttribute( flag ) && !( node.getAttribute( flag ) || '' ).trim() ) {
                    r.push( node );
                }
            }
            return r;
        } ,
        
        "childNodes" : function ( nodelist , flag ) {
            for ( var i = 0 , l = nodelist.length , r = [] ; i < l ; i++ ) {
                if ( nodelist[ i ].firstChild ) {
                    concat.apply( r , applyToolsOnNodelist( childrenNodes( nodelist[ i ] ) , flag ) );
                }
            }
            return r;
        } ,
        
        "parentNode" : function ( nodelist , flag ) {
            for ( var i = 0 , l = nodelist.length , r = [] , node ; i < l ; i++ ) {
                if ( isnode( node = nodelist[ i ].parentNode ) ) {
                    r.push( node );
                }
            }
            return applyToolsOnNodelist( r , flag );
        } ,
        
        "nextChildNodes" : function ( nodelist , flag ) {
            for ( var i = 0 , l = nodelist.length , r = [] , node ; i < l ; i++ ) {
                node = nodelist[ i ].nextElementSibling;
                while ( node ) {
                    r.push( node );
                    node = node.nextElementSibling;
                }
            }
            return applyToolsOnNodelist( r , flag );
        } ,
        
        "nextSibling" : function ( nodelist , flag ) {
            for ( var i = 0 , l = nodelist.length , r = [] , node ; i < l ; i++ ) {
                if ( node = nodelist[ i ].nextElementSibling ) {
                    r.push( node );
                }
            }
            return applyToolsOnNodelist( r , flag );
        }
        
    };
    
    // css3 selector
    var crossBrowser = {
        
        'read-only' : function ( nodelist ) {
            for ( var i = 0 , l = nodelist.length , r = [] , t ; i < l ; i++ ) {
                t = nodelist[ i ];
                t.readOnly === true && r.push( t );
            }
            return r;
        } ,
        
        'read-write' : function ( nodelist ) {
            for ( var i = 0 , l = nodelist.length , r = [] , t ; i < l ; i++ ) {
                t = nodelist[ i ];
                t.readOnly === false && r.push( t );
            }
            return r;
        } ,
        
        'in-range' : function ( nodelist ) {
            var i = 0 , l = nodelist.length , r = [] ,
                min , max , val , Emin , Emax;
            
            for ( ; i < l ; i++ ) {
                min = nodelist[ i ].getAttribute( 'min' );
                max = nodelist[ i ].getAttribute( 'max' );
                val = nodelist[ i ].value;
                Emin = exist( min );
                Emax = exist( max );
                
                if ( exist( val ) && ( ( Emin && +val >= +min ) || !Emin ) && ( ( Emax && +val <= +max ) || !Emax ) ) {
                    r.push( nodelist[ i ] );
                }
            }
            
            return r;
        } ,
        
        'out-of-range' : function ( nodelist ) {
            var i = 0 , l = nodelist.length , r = [] ,
                min , max , val , Emin , Emax;
            
            for ( ; i < l ; i++ ) {
                min = nodelist[ i ].getAttribute( 'min' );
                max = nodelist[ i ].getAttribute( 'max' );
                val = nodelist[ i ].value;
                Emin = exist( min );
                Emax = exist( max );
                
                if ( exist( val ) && ( ( Emin && +val < +min ) || !Emin ) && ( ( Emax && +val > +max ) || !Emax ) ) {
                    r.push( nodelist[ i ] );
                }
            }
            
            return r;
        }
        
    };
    
    var compiler = ( function () {
        var compiler = cacheHandler() ,
            cacheNormalize = cacheHandler() ,
            
            // when the script load, check all supported css3 tool, if isn't, add the function in targetJS features object
            reg = ( function () {
                var reg = [] , join;
                
                forin( crossBrowser , function ( selector , polyfill ) {
                    if ( !support( selector ) ) {
                        advSelector[ selector ] = polyfill;
                    }
                } );
                
                forin( advSelector , function ( selector ) {
                    reg.push( selector );
                } );
                
                join = reg.join( '|' );
                
                return {
                    tool : new RegExp( '^(' + join + ')$' , 'g' ) ,
                    check : new RegExp( '((:)(' + join + '))' , 'g' )
                };
            } )() ,
            
            // check if selector is supported natively in :not()
            notSupport = ( function () {
                function support ( flag ) {
                    try {
                        return document.querySelector( 'html:not(' + flag + ')' ), true;
                    } catch ( _ ) {
                        return false;
                    }
                }
                
                return !support( 'body' ) ? function () {
                    return false;
                } : function ( flag ) {
                    return support( flag );
                };
            } )() ,
            
            // ex :
            // "p[!id]" become "p:not([id])"
            // "p[-id]" become "p:fullAttr([id])"
            // "p[!-id]" become "p:emptyAttr([id])"
            // "p:not(a,div)" become "p:not(a):not(div)"
            // ":first < :last" become ":first:parent(:last)"
            // ":first > :last" become ":first:children(:last)"
            // "p:not(a)[id!=toto]" become "p:not(a):not([id=toto])"
            beautify = ( function () {
                var notEq = /(![=]{0,2})/gi ,
                    notEqB = /(\[![\w-]+])/gi ,
                    fullAttr = /(\[-[\w-]+])/gi ,
                    emptyAttr = /(\[!-[\w-]+])/gi ,
                    selec = '[* .:"\'\\\{}=~|\\\^\\\$\\\+#><a-z0-9\\\[\\\]-]+' ,
                    selecAdv = '[* .:"\'\\\{})(=~|\\\^\\\$\\\+#><a-z0-9\\\[\\\]-]+' ,
                    toEq = new RegExp( '(\\\[[\\\w-]+![=]{0,2}' + selec + '])' , 'gi' ) ,
                    multiSel = new RegExp( '(:not\\\()(' + selecAdv + ')([,]+' + selecAdv + '[,]?)+(\\\))' , 'gi' );
                
                function selToNot ( ar ) {
                    for ( var tmp , i = 0 , result = '' , l = ar.length ; i < l ; i++ ) {
                        ( tmp = ar[ i ].trim() ) && ( result += ':not(' + tmp + ')' );
                    }
                    return result;
                }
                
                return function ( selector ) {
                    return extractSpec( selector.trim() )
                    // !=
                        .replace( toEq , function ( value ) {
                            return ':not(' + value.replace( notEq , '=' ) + ')';
                        } )
                        // [!-attr]
                        .replace( emptyAttr , function ( value ) {
                            return ':emptyAttr(' + value.slice( 3 , -1 ) + ')';
                        } )
                        // [-attr]
                        .replace( fullAttr , function ( value ) {
                            return ':fullAttr(' + value.slice( 2 , -1 ) + ')';
                        } )
                        // [!attr]
                        .replace( notEqB , function ( value ) {
                            return ':not([' + value.slice( 2 ) + ')';
                        } )
                        // multi not selector
                        .replace( multiSel , function ( value ) {
                            return selToNot( value.slice( 5 , -1 ).split( ',' ) );
                        } );
                };
            } )() ,
            
            // split selector
            // first by comma
            // second by space if it's necessary ( ex : "div p" isn't splited but "div p:first" become ["div","p:first"]
            splitSelector = ( function () {
                function commatSplit ( selector ) {
                    var i = 0 ,
                        ar = clonearray( multiIndexOf( ',' , selector ) ).reverse() ,
                        l = ar.length , cur = 0 , selectors = [] , tmp;
                    
                    for ( ; i < l ; i++ ) {
                        if ( tmp = selector.slice( cur , ar[ i ] ).trim() ) {
                            selectors.push( tmp );
                            cur = ar[ i ] + 1;
                        }
                    }
                    
                    if ( tmp = selector.slice( cur ).trim() ) {
                        selectors.push( tmp );
                    }
                    
                    return selectors;
                }
                
                function spaceSplit ( selectors , parent ) {
                    var i = 0 , l = selectors.length , ar , cur = 0 ,
                        tmp , i2 , l2 , tmp2 , child , sel ,
                        spc = /[\s]*([~+><])[\s]*/g ,
                        beg = /^[\s]?[:\[]/g ,
                        reg = /[:)\[]+/g;
                    
                    for ( ; i < l ; i++ ) {
                        
                        ar = [];
                        child = '';
                        i2 = cur = 0;
                        
                        sel = selectors[ i ].replace( spc , function ( s ) {
                            return s.trim();
                        } );
                        tmp = clonearray( multiIndexOf( ' ' , sel ) ).reverse();
                        l2 = tmp.length;
                        
                        // TODO do...while instead of... that !
                        for ( ; i2 < l2 ; i2++ ) {
                            if ( tmp2 = sel.slice( cur , tmp[ i2 ] ).trim() ) {
                                
                                if ( resetregexp( reg ).exec( tmp2 ) ) {
                                    
                                    if ( resetregexp( beg ).exec( tmp2 = tmp2.trim() ) ) {
                                        tmp2 = parent + tmp2;
                                    }
                                    
                                    !!child && ( ar.push( child ), child = '' );
                                    
                                    ar.push( tmp2 );
                                }
                                else {
                                    child += ' ' + tmp2;
                                }
                                
                                cur = tmp[ i2 ] + 1;
                            }
                        }
                        
                        if ( tmp2 = sel.slice( cur ).trim() ) {
                            
                            if ( resetregexp( reg ).exec( tmp2 ) ) {
                                
                                if ( resetregexp( beg ).exec( tmp2 = tmp2.trim() ) ) {
                                    tmp2 = parent + tmp2;
                                }
                                
                                !!child && ( ar.push( child ), child = '' );
                                
                                ar.push( tmp2 );
                            }
                            else {
                                child += ' ' + tmp2;
                            }
                            
                        }
                        
                        ( !ar.length || !!child ) && ar.push( child );
                        selectors[ i ] = ar;
                    }
                    
                    return selectors;
                }
                
                return function ( str , parent ) {
                    return spaceSplit( commatSplit( normalize( str ) , parent ) , parent );
                };
            } )() ,
            
            // input shortcut
            inputShortcut = ( function ( i , type ) {
                return new RegExp( '((:)(' + type.join( '|' ) + '))' , 'gi' );
            } )( 0 , [
                'checkbox' , 'text' , 'submit' , 'file' , 'email' , 'reset' , 'radio' , 'password' ,
                'hidden' , 'color' , 'date' , 'datetime' , 'datetime-local' , 'image' , 'month' , 'number' ,
                'range' , 'search' , 'tel' , 'time' , 'url' , 'week'
            ] , '' ) ,
            
            // css shortcut
            toolShortcut = ( function ( tool , reg ) {
                for ( var i in tool ) {
                    reg += i + '|';
                }
                reg = RegExp( '((:)(' + reg.slice( 0 , -1 ) + '))' , 'gi' );
                
                function slice ( v ) { return tool[ v.slice( 1 ) ]; }
                
                return function ( str ) {
                    return str.replace( reg , slice );
                };
            } )( {
                odd : ':nth-child(odd)' ,
                even : ':nth-child(even)'
            } , '' ) ,
            
            chkEnd = /[~>+<][\s]*$/g ,
            alpha = /[\w-]+/gi ,
            spec = /[><+~:]/gi;
        
        // remove useless space, in and around the string
        function normalize ( str ) {
            if ( cacheNormalize[ str ] ) {
                return cacheNormalize[ str ];
            }
            
            cacheNormalize( str , str.replace( /[\s\u00A0]+/g , ' ' ).trim() );
            
            return cacheNormalize[ str ];
        }
        
        function switchTool ( char ) {
            var tool = null;
            
            switch ( char ) {
                case '>':
                    tool = 'childNodes';
                    break;
                
                case '<':
                    tool = 'parentNode';
                    break;
                
                case '~':
                    tool = 'nextChildNodes';
                    break;
                
                case '+':
                    tool = 'nextSibling';
                    break;
            }
            
            return tool;
        }
        
        // parsing selector for ">", "<", "~", "+"
        // use native selector if it's possible
        function extractSpec ( selector ) {
            var i = 0 , l = selector.length ,
                spc = /[><~+]/g , Hspc = /[<]/g ,
                o = 0 , index = [] , r = [] , curIndex , befIndex ,
                before , after , chk = reg.check , last = {} , laststack = [] , tool , char;
            
            for ( ; i < l ; i++ ) {
                char = selector.charAt( i );
                if ( char === '(' ) {
                    o++;
                }
                else if ( char === ')' ) {
                    o--;
                }
                else if ( resetregexp( spc ).exec( char ) && !o ) {
                    index.push( {
                        index : i + 1 ,
                        char : char ,
                        h : resetregexp( Hspc ).exec( char )
                    } );
                }
            }
            
            for ( i = 0, index.reverse(), l = index.length ; i < l ; i++ ) {
                curIndex = index[ i ].index;
                char = index[ i ].char;
                
                befIndex = index[ i + 1 ] ? index[ i + 1 ].index : 0;
                
                before = selector.slice( befIndex , curIndex - 1 ).trim();
                
                after = selector.slice( curIndex ).trim();
                
                selector = selector.slice( 0 , curIndex - 1 ).trim();
                
                // standard selector, check input shortcut and goto next
                if ( !resetregexp( chk ).exec( before ) && !resetregexp( chk ).exec( after ) && !index[ i ].h ) {
                    
                    r.unshift( char + after.replace( inputShortcut , function ( a ) {
                        return ' input[type="' + a.slice( 1 ) + '"]';
                    } ) );
                    
                    last = {
                        char : char ,
                        after : after ,
                        packed : false
                    };
                    
                    laststack.unshift( last );
                    
                    continue;
                }
                
                // complex selector, switch to manual flag treatment
                if ( tool = switchTool( char ) ) {
                    r.unshift( ':' + tool + '(' + after.trim() + ')' );
                    
                    last = {
                        char : char ,
                        after : after ,
                        packed : true
                    };
                    
                    laststack.unshift( last );
                    
                    // check last tool
                    // CAREFUL BETA
                    ( function ( i ) {
                        while ( r[ i ] && laststack[ i ] ) {
                            
                            if ( !laststack[ i ].packed ) {
                                
                                if ( tool = switchTool( laststack[ i ].char ) ) {
                                    r[ i ] = ':' + tool + '(' + laststack[ i ].after.trim() + ')';
                                    laststack[ i ].packed = true;
                                }
                                
                            }
                            else {
                                break;
                            }
                            
                            i++;
                        }
                    } )( 1 );
                    
                    continue;
                }
                
                last = null;
            }
            
            return normalize( selector + r.join( '' ) );
        }
        
        function badEnd ( s ) {
            return resetregexp( chkEnd ).exec( s ) ? s + '*' : s;
        }
        
        function checkTool ( selector ) {
            var b = 0 , l , i , i2 , ar , tmp , flag , tool ,
                after , before , toolName , rest , r = [] , exp = reg.tool;
            
            // parse and replace input shortcut
            selector = selector.replace( inputShortcut , function ( a ) {
                return ' input[type="' + a.slice( 1 ) + '"]';
            } );
            
            // parse and replace selector shortcut
            selector = toolShortcut( selector );
            
            // get all tools index
            ar = multiIndexOf( ':' , selector );
            
            for ( l = ar.length ; b < l ; b++ ) {
                // tool ( with argument )
                tool = selector.slice( ar[ b ] + 1 );
                
                if ( tmp = firstIndexOf( resetregexp( spec ) , tool ) ) {
                    tool = tool.slice( 0 , tmp );
                }
                
                // tool name ( without argument )
                toolName = resetregexp( alpha ).exec( tool )[ 0 ];
                
                // selector before tool
                before = selector.slice( 0 , ar[ b ] );
                
                // selector after tool
                after = selector.slice( ar[ b ] + tool.length + 1 );
                
                // argument
                if ( ( i = tool.indexOf( '(' ), i ) >= 0 ) {
                    i2 = findCloser( tool );
                    flag = tool.slice( i + 1 , i2 ).trim();
                    rest = tool.slice( i2 + 1 ).trim();
                }
                else {
                    flag = null;
                    rest = '';
                }
                
                // if toolName isn't natively supported, edit current selector and save tool for manual filter
                if ( ( toolName !== 'not' || !notSupport( flag ) ) && resetregexp( exp ).exec( toolName ) ) {
                    
                    selector = before + rest + after;
                    r.push( { name : toolName , flag : flag } );
                    
                }
            }
            
            return [ normalize( badEnd( selector ) ) || '*' , r.reverse() ];
        }
        
        return function ( selector , parent , fus ) {
            var i , l , i2 , l2 , child , splited;
            
            if ( compiler[ fus ] ) {
                return compiler[ fus ];
            }
            
            splited = splitSelector( selector , parent );
            
            for ( i = 0, l = splited.length ; i < l ; i++ ) {
                child = splited[ i ];
                
                for ( i2 = 0, l2 = child.length ; i2 < l2 ; i2++ ) {
                    child[ i2 ] = checkTool( beautify( child[ i2 ] ) );
                }
            }
            
            return compiler( fus , splited ), splited;
        };
    } )();
    
    // call one after one all tool unsupported natively and redefined nodelist for each
    function advanceRequest ( object ) {
        for ( var i = 0 , t = object.tool , n = object.nodelist , l = t.length , _ ; i < l ; i++ ) {
            if ( !n || !n.length ) {
                break;
            }
            
            _ = t[ i ];
            
            if ( advSelector[ _.name ] ) {
                n = advSelector[ _.name ]( n , _.flag , object );
            }
        }
        return n || [];
    }
    
    function findChild ( nodelist , selector ) {
        for ( var i = 0 , l = nodelist.length , r = [] ; i < l ; ) {
            concat.apply( r , advanceRequest( {
                nodelist : query( selector[ 0 ] , nodelist[ i ] ) ,
                container : nodelist[ i++ ] ,
                selector : selector[ 0 ] ,
                tool : selector[ 1 ]
            } ) );
        }
        return r;
    }
    
    function deepfind ( firstTool , toolList , nodelist , container ) {
        var tmp = advanceRequest( {
            tool : firstTool[ 1 ] ,
            container : container ,
            selector : firstTool[ 0 ] ,
            nodelist : nodelist || query( firstTool[ 0 ] , container )
        } );
        
        for ( var i = 1 , l = toolList.length ; i < l ; ) {
            tmp = findChild( tmp , toolList[ i++ ] );
        }
        
        return tmp;
    }
    
    function browseCompiledList ( list , nodelist , container ) {
        for ( var i = 0 , r = [] , l = list.length , compiledList ; i < l ; i++ ) {
            compiledList = list[ i ];
            concat.apply( r , deepfind( compiledList[ 0 ] , compiledList , nodelist , container ) );
        }
        return r;
    }
    
    function find ( selector , container , parent ) {
        var r , f , p = parent || '*' , ss = selector + p;
        
        if ( ( f = findCache[ ss ] ) && f.c === container ) {
            return f.r;
        }
        
        level++;
        
        r = arrayunique( browseCompiledList( compiler( selector , p , ss ) , null , container ) );
        
        if ( --level > 0 ) {
            findCache( ss , { c : container , r : r } );
        }
        else {
            findCache = cacheHandler();
        }
        
        return r;
    }
    
    // targetJS
    var _T = function ( s , o ) {
        if ( isstring( s ) ) {
            return find( s , isnode( o ) ? o : document );
        }
        
        return _T.filter( s , o );
    };
    
    // filter nodelist
    _T.filter = function ( nodelist , filter , global ) {
        if ( isnodelist( nodelist ) || isarray( nodelist ) ) {
            nodelist = typedtoarray( nodelist );
            
            if ( isstring( filter ) ) {
                if ( global ) {
                    return globalNodelistFilter( nodelist , filter );
                }
                
                return nodelistFilter( nodelist , filter );
            }
            
            if ( isnode( filter ) ) {
                return onlyChildsOf( filter , nodelist );
            }
            
            return nodelist;
        }
        
        return null;
    };
    
    // find nodes from nodelist
    _T.find = function ( nodelist , selector ) {
        var r = [] , compiled = compiler( selector , '*' , selector + '*' );
        
        for ( var i = 0 , l = nodelist.length , node ; i < l ; i++ ) {
            node = nodelist[ i ];
            concat.apply( r , browseCompiledList( compiled , null , node ) );
        }
        
        if ( level <= 0 ) {
            findCache = cacheHandler();
        }
        
        return arrayunique( r );
    };
    
    // find nodes from nodelist
    _T.apply = function ( nodelist , selector , container ) {
        var r = [] , compiled = compiler( selector , '*' , selector + '*' );
        
        concat.apply( r , browseCompiledList( compiled , nodelist , container || document ) );
        
        if ( level <= 0 ) {
            findCache = cacheHandler();
        }
        
        return arrayunique( r );
    };
    
    // find nodes from nodelist
    _T.applyEach = function ( nodelist , selector ) {
        var r = [] , compiled = compiler( selector , '*' , selector + '*' );
        
        for ( var i = 0 , l = nodelist.length , node ; i < l ; i++ ) {
            node = nodelist[ i ];
            concat.apply( r , browseCompiledList( compiled , [ node ] , node.parentNode ) );
        }
        
        if ( level <= 0 ) {
            findCache = cacheHandler();
        }
        
        return arrayunique( r );
    };
    
    // css match
    // use native function if it's possible
    _T.match = _tmatch = ( function () {
        function man ( e , s , b ) {
            return inarray( e , find( s , b || getdocument( e ) ) );
        }
        
        return function ( e , s , b ) {
            try {
                return matchesSelector( e , s );
            } catch ( _ ) {
                return man( e , s , b );
            }
        };
    } )();
    
    // HTMLElement.closest
    // use native function if it's possible
    _T.closest = _tclosest = ( function () {
        
        function man ( element , selector ) {
            
            if ( _tmatch( element , selector ) ) {
                return element;
            }
            
            while ( isnode( element = element.parentNode ) ) {
                if ( _tmatch( element , selector ) ) {
                    return element;
                }
            }
            
            return null;
        }
        
        return function ( element , selector ) {
            try {
                return closest( element , selector );
            } catch ( _ ) {
                return man( element , selector );
            }
            
        };
    } )();
    
    /**
     * @public
     * @readonly
     * @function _T
     * */
    _defineProperty( global , '_T' , _T );
    
    /**
     * @public
     * @readonly
     * @function target
     * */
    _defineProperty( global , 'target' , _T );
    
    
} )();